import * as config from "./config";

let users = [];

const connectedUsers = new Map();

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;
    console.log(username + " connected");

    if (users.includes(username)) {
      socket.emit("USER_EXISTS", username);
    } else {
      users.push(username);
      socket.emit("RENDER");
    }

    socket.on("disconnect", () => {
      users = users.map((item) => {
        if (item != username) {
          return item;
        }
      });
    });

    socket.on("JOIN_ROOM", (data) => {
      const { roomID, nameuser } = data;
      socket.join(roomID);
      console.log(`room '${roomID}' started`);
      connectedUsers.set(nameuser, roomID);
      console.log(connectedUsers);
      let transitString = JSON.stringify(Array.from(connectedUsers));
      const resdata = { arr: transitString, room: roomID };
      io.to(roomID).emit("UPDATE_ROOM", resdata);
    });

    socket.on("CREATE_BLOCK", (name) => {
      io.emit("CREATE_NEW_ROOM", name);
    });

    socket.on("EXIT_ROOM", (obj) => {
      const { sid, user, roomID } = obj;
      socket.join(sid);
      io.emit("USER_LEFT", user);
      connectedUsers.delete(user);
      console.log(connectedUsers);
      let transitString = JSON.stringify(Array.from(connectedUsers));
      const resdata = { arr: transitString, room: roomID };
      io.emit("UPDATE_ROOM", resdata);
    });
  });
};
