import { gameProcess } from "./gameProcess.js";

const username = sessionStorage.getItem("username");
//MAIN BLOCKS
const room_page = document.getElementById("rooms-page");
const game_page = document.getElementById("game-page");

//room_page
const room_page_box_cards = document.createElement("div");
room_page_box_cards.classList.add("roomblocks");

//game_page
const statuses = document.createElement("div");
const container = document.createElement("div");

const divInfoBlock = document.createElement("div");
const divGameBlock = document.createElement("div");
const divCommBlock = document.createElement("div");
divGameBlock.classList.add("divGameBlock");
container.classList.add("container");

if (!username) {
  window.location.replace("/login");
}
//Facade pattern коментатора
class commentator {
  constructor(element) {
    this.commentator = element;
    this.commentator.textContent =
      "Welcome, i'm your commentator for today's match";
    this.commentator.id = "commentator";
    addClasses(this.commentator, "commentator");
    this.addPhoto();
  }

  addPhoto() {
    const imgComm = document.createElement("img");
    imgComm.src = "../img/mrRobot.png";
    imgComm.classList.add("img-comm");
    appendAllTo(this.commentator, imgComm);
  }

  itsJokeTime(joke) {
    let randNum = getRandomInt(6);
    let message = joke[randNum];
    this.saySomething(message);
  }

  positionCheck(progressTransfer) {
    let progress = new Map(JSON.parse(progressTransfer));

    let arrProgress = Array.from(progress, ([name, value]) => ({
      name,
      value,
    }));

    let highestPercent = [];

    for (let i = 0; i < arrProgress.length; i++) {
      highestPercent.push(arrProgress[i].value);
    }

    let message = "Positions for now ";

    let positions = [];

    highestPercent.sort(function (a, b) {
      return b - a;
    });

    let difference = 0;

    if (highestPercent[1]) {
      difference = 100 * highestPercent[0] - 100 * highestPercent[1];
      difference = Math.floor(difference);
    }

    for (let i = 0; i < highestPercent.length; i++) {
      for (let k = 0; k < highestPercent.length; k++) {
        if (highestPercent[i] == arrProgress[k].value) {
          positions.push(arrProgress[k].name);
        }
      }
    }

    const MAX_PLAYERS_TO_SHOW = 3;

    let topPositionsNumber = Math.min(MAX_PLAYERS_TO_SHOW, positions.length);
    let messagePart2 = "";
    for (let i = 0; i < topPositionsNumber; i++) {
      messagePart2 =
        messagePart2 + "on " + (i + 1) + " place is " + positions[i] + " ,";
    }
    message = message + messagePart2;
    message =
      message + " the difference between leaders is " + difference + " percent";
    this.saySomething(message);
  }

  playerAlmostOnFinish(player) {
    let message = player + " is almost on finish!";
    this.saySomething(message);
  }

  playerFinished(player) {
    let message = player + " finished!";
    this.saySomething(message);
  }

  hideElement() {
    this.commentator.classList.add("display-none");
  }

  showElement() {
    this.commentator.classList.remove("display-none");
  }

  getElement() {
    return this.commentator;
  }

  saySomething(message) {
    this.commentator.textContent = message;
    this.addPhoto();
  }
}

//commentator init
const blockComm = document.createElement("div");
let comment = new commentator(blockComm);
comment.hideElement();

//Factory pattern, я його зрозумів і знаю як використовувати,
//але не знаю чи доцільно його використовувати тут(ці коментарі призначенні тільки,
//для показу результату (я знаю що таке небажанно писати по правилах CleanCode);
class ElementsHTML {
  createElement(type) {
    let element;
    if (type == "div") {
      element = new DivElement();
    } else if (type == "img") {
      element = new ImgElement();
    } else if (type == "span") {
      element = new SpanElement();
    } else if (type == "p") {
      element = new pElement();
    }
    element.type = type;
  }
}

class DivElement {
  constructor() {
    this.block = document.createElement("div");
  }
}

class ImgElement {
  constructor() {
    this.block = document.createElement("img");
  }
}

class SpanElement {
  constructor() {
    this.block = document.createElement("span");
  }
}

class pElement {
  constructor() {
    this.block = document.createElement("p");
  }
}

//Pure Functions
function getElementWithId(id) {
  return document.getElementById(id);
}

function setDisplayNone(element) {
  return element.classList.add("display-none");
}

function setDisplayOn(element) {
  return element.classList.remove("display-none");
}

function appendAllTo(parent, ...child) {
  let len = child.length;
  for (let i = 0; i < len; i++) {
    parent.appendChild(child[i]);
  }
}

function addClasses(element, ...classes) {
  let len = classes.length;
  for (let i = 0; i < len; i++) {
    element.classList.add(classes[i]);
  }
}

function addStrings(firstString, secondString) {
  return firstString + secondString;
}

//Currying function
let addStringsCurry = _.curry(addStrings);

export const socket = io("", { query: { username } });

const showRoomPage = () => {
  const NamePage = document.createElement("div");
  addClasses(NamePage, "looby_name");
  NamePage.textContent = "Lobby";

  const ButAdd = document.createElement("button");
  addClasses(ButAdd, "btn");
  ButAdd.textContent = "Create Room";

  ButAdd.addEventListener("click", () => {
    const nameNewRoom = prompt("Enter new room");
    if (nameNewRoom != "" || nameNewRoom != null) {
      socket.emit("REQUEST_NEW_ROOM", nameNewRoom);
    }
  });
  appendAllTo(room_page, NamePage, ButAdd);
};

const clearRoomPage = () => {
  room_page.innerHTML = " ";
  showRoomPage();
};

const renderRoom = (roomname) => {
  showGamePageDisplay(roomname);
};

const createNewRoom = (data) => {
  const { name, isValid } = data;
  if (isValid) {
    appendAllTo(room_page, room_page_box_cards);
    const divBlock = document.createElement("div");
    addClasses(divBlock, "room", "blockGame", name);
    let idBl = name + "blockRoom";
    divBlock.id = idBl;

    const header = document.createElement("div");
    addClasses(header, "headerGame");
    let isNa = name + "header";
    header.id = isNa;
    header.textContent = "0 user connected";

    const nameRoom = document.createElement("div");
    addClasses(nameRoom, "nameRoom");
    nameRoom.innerHTML = name;

    const buttonCont = document.createElement("button");
    addClasses(buttonCont, "btn", "join-btn");
    buttonCont.textContent = "Join Room";
    buttonCont.addEventListener("click", function () {
      joinRoom(name);
    });

    appendAllTo(divBlock, header, nameRoom, buttonCont);
    appendAllTo(room_page_box_cards, divBlock);
  } else {
    alert("Name error");
  }
};

const joinNOW = (roomname) => {
  joinRoom(roomname);
};

const joinRoom = (roomname) => {
  socket.emit("REQUEST_JOIN_ROOM", roomname);
};

const fullRoom = (Room) => {
  alert(`Room ${Room} is full`);
};

const createGamePage = (roomname) => {
  const nameroom = document.createElement("div");
  addClasses(nameroom, "looby_name");
  nameroom.textContent = roomname;

  const buttonBack = document.createElement("button");
  addClasses(buttonBack, "btn", "btn-back");
  buttonBack.textContent = "Back To Rooms";
  buttonBack.id = "quit-room-btn";

  buttonBack.addEventListener("click", function () {
    removeAllChildNodes(container);
    exitRoom(roomname);
  });

  appendAllTo(divInfoBlock, nameroom, buttonBack);
  appendAllTo(container, divInfoBlock);

  createGameSpace(roomname);
  createCommentator();

  appendAllTo(game_page, container);
};

function createCommentator() {
  comment.showElement();
  appendAllTo(divCommBlock, blockComm);
  appendAllTo(container, divCommBlock);
}

function createGameSpace(roomname) {
  const gameSpace = document.createElement("div");
  addClasses(gameSpace, "gamespace");
  gameSpace.id = "gamespace";

  const readyBut = document.createElement("button");
  addClasses(readyBut, "btn");
  readyBut.id = "ready-btn";
  readyBut.textContent = "Ready";
  readyBut.addEventListener("click", function () {
    ready(roomname, "ready-btn");
  });

  appendAllTo(gameSpace, readyBut);
  appendAllTo(divGameBlock, gameSpace);
  appendAllTo(container, divGameBlock);
}

const ready = (roomname, newClass) => {
  const but = getElementWithId(newClass);
  const data = { roomname: roomname, nameuser: username };

  if (but.textContent == "Ready") {
    but.textContent = "Not Ready";
    socket.emit("READY_ONE", data);
  } else {
    but.textContent = "Ready";
    socket.emit("UNREADY_ONE", data);
  }
};

const updateStatus = (data) => {
  let { room, session, transferProgress } = data;
  cleanStatus();
  let progress;

  progress = new Map(JSON.parse(transferProgress));

  if (!progress.has(username)) {
    progress.set(username, 0);
  }

  let userSession = new Map(JSON.parse(session));

  userSession.forEach((value, key) => {
    if (value == room) {
      let progressValue = progress.get(key);
      createStatus(key, progressValue);
    }
  });
};

const destroyRoom = (roomname) => {
  let idBl = roomname + "blockRoom";
  const roomBlock = getElementWithId(idBl);
  setDisplayNone(roomBlock);
};

const reloadOnline = (num) => {
  const { room, online } = num;
  let id = room + "header";
  const header = getElementWithId(id);
  header.textContent = `${online} user(s) connected`;
};

const createStatus = (userKey, progressValue) => {
  const statusBlock = document.createElement("div");
  addClasses(statusBlock, userKey);

  const upStatus = document.createElement("div");
  addClasses(upStatus, "upstatus");

  const indicator = document.createElement("div");
  addClasses(indicator, "ready-status-red");
  indicator.id = userKey;

  const nameStatus = document.createElement("div");
  if (userKey == username) {
    nameStatus.textContent = userKey + " (you)";
  } else {
    nameStatus.textContent = userKey;
  }

  appendAllTo(upStatus, indicator, nameStatus);

  const bar = document.createElement("div");
  addClasses(bar, "user-progress");
  bar.id = userKey + "bar";

  const barAfter = document.createElement("div");
  barAfter.id = username + barAfter;
  barAfter.classList.add("bar-after");

  progressValue = progressValue * 100;

  let width = addStringsCurry(progressValue)("%");

  barAfter.style.width = width;

  appendAllTo(bar, barAfter);
  appendAllTo(statusBlock, upStatus, bar);
  appendAllTo(statuses, statusBlock);
  appendAllTo(divInfoBlock, statuses);
};

const cleanStatus = () => {
  statuses.innerHTML = "";
};

const updateStatusIndGreen = (user) => {
  const status = getElementWithId(user);
  status.classList.remove("ready-status-red");
  addClasses(status, "ready-status-green");
};

const updateStatusIndRed = (user) => {
  const status = getElementWithId(user);
  status.classList.remove("ready-status-green");
  addClasses(status, "ready-status-red");
};

const updateStatusIndGreenAll = (data) => {
  const { roomname, transfer } = data;
  let mapUser = new Map(JSON.parse(transfer));

  mapUser.forEach((value, key) => {
    if (value == roomname) {
      updateStatusIndGreen(key);
    }
  });
};

const updateStatusIndRedAll = (data) => {
  const { roomname, transfer } = data;
  let mapUser = new Map(JSON.parse(transfer));

  mapUser.forEach((value, key) => {
    if (value == roomname) {
      updateStatusIndRed(key);
    }
  });
};

function removeAllChildNodes(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}

const exitRoom = (roomname) => {
  divInfoBlock.innerHTML = "";
  divGameBlock.innerHTML = "";
  showRoomPageDisplay(roomname);
  socket.emit("EXIT_ROOM", roomname);
};

const showGamePageDisplay = (roomname) => {
  createGamePage(roomname);
  setDisplayNone(room_page);
  setDisplayOn(game_page);
};

const showRoomPageDisplay = (roomname) => {
  setDisplayNone(game_page);
  setDisplayOn(room_page);
};

function commentatorSays(message) {
  const comment = getElementWithId("commentator");
  comment.textContent = message;
  const imgComm = document.createElement("img");
  imgComm.src = "../img/mrRobot.png";
  imgComm.classList.add("img-comm");
  appendAllTo(comment, imgComm);
}

function commentatorPosition(progressTransfer) {
  comment.positionCheck(progressTransfer);
}

function notifyAboutPlayerToWin(player) {
  comment.playerAlmostOnFinish(player);
}

function notifyAboutPlayerFinished(player) {
  comment.playerFinished(player);
}

function jokeTime(jokes) {
  comment.itsJokeTime(jokes);
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

const userExists = (userDuplicate) => {
  window.location.replace("/login");
  alert(`User ${userDuplicate} already exists`);
};

function commentatorIntrodude(users, room) {
  let message = "Our racers for today is ";
  let usersTr = new Map(JSON.parse(users));
  usersTr.forEach((value, key) => {
    console.log("value: " + value + " room: " + room);
    if (value == room) {
      message = message + key + ", ";
    }
  });

  commentatorSays(message);
}

function startingTheGame(data) {
  const { users, roomname } = data;
  commentatorIntrodude(users, roomname);

  const butBack = getElementWithId("quit-room-btn");
  setDisplayNone(butBack);

  const button = getElementWithId("ready-btn");
  setDisplayNone(button);

  const timer = document.createElement("div");
  addClasses(timer, "timer-before");

  const gamespace = getElementWithId("gamespace");
  appendAllTo(gamespace, timer);

  const { timerSec } = data;
  countDown(timerSec, timer, data);
}

function countDown(seconds, element, data) {
  let timeLeft = seconds;
  const interval = setInterval(() => {
    if (timeLeft <= 0) {
      setDisplayNone(element);
      clearInterval(interval);
      startingTheGamea(data);
    }
    element.innerText = timeLeft--;
  }, 1000);
}

const startingTheGamea = (data) => {
  const { roomname, timerSec, gameSec, text } = data;

  const timer = document.createElement("div");
  addClasses(timer, "timer");
  timer.id = "timer";

  const container = document.createElement("div");
  addClasses(container, "container_game");

  const display = document.createElement("div");
  addClasses(display, "quote-display");
  display.id = "quoteDisplay";
  display.textContent = "quote";

  const textArea = document.createElement("textarea");
  textArea.focus();
  textArea.id = "quoteInput";
  addClasses(textArea, "quote-input");

  const gamebox = getElementWithId("gamespace");

  appendAllTo(container, display, textArea);
  appendAllTo(gamebox, timer, container);
  let dataBlocks = { display: display, textArea: textArea, timer };
  let dataConst = { timerTime: timerSec, gameTime: gameSec };
  gameProcess(dataBlocks, dataConst, text, socket, username, roomname);
};

const showWinners = (data) => {
  comment.saySomething("CONGRATULATIONS!");

  const { roomname, winners } = data;

  socket.emit("CLEAN_PROGRESS", roomname);

  const gamespace = getElementWithId("gamespace");
  gamespace.innerHTML = "";

  const containerTable = document.createElement("div");
  addClasses(containerTable, "containerTable");

  const title = document.createElement("div");
  addClasses(containerTable, "containerTable");
  addClasses(title, "title-winner");

  const tableTitle = document.createElement("span");
  tableTitle.textContent = "Game Results";

  const button = document.createElement("button");
  button.addEventListener("click", () => {
    const butBack = getElementWithId("quit-room-btn");
    butBack.classList.remove("display-none");
    cleanWinners(roomname);
  });
  button.textContent = "X ";

  appendAllTo(title, tableTitle, button);

  const table = document.createElement("div");
  addClasses(table, "table-winner");
  winners.map((val, index) => {
    const spaceUser = document.createElement("div");
    addClasses(spaceUser, "spaceuser");
    const text = index + 1 + " -> " + val;
    spaceUser.textContent = text;
    appendAllTo(table, spaceUser);
  });

  appendAllTo(containerTable, title, table);

  appendAllTo(gamespace, containerTable);
};

function cleanWinners(roomname) {
  divGameBlock.innerHTML = "";
  comment.hideElement();
  createGameSpace(roomname);
  createCommentator();
}

//UI
socket.on("DUPLICATE_USER", userExists);
socket.on("SHOW_ROOM_PAGE", clearRoomPage);
socket.on("CREATE_NEW_ROOM", createNewRoom);
socket.on("DESTROY_ROOM", destroyRoom);
socket.on("FULL_ROOM", fullRoom);
socket.on("JOIN_NOW", joinNOW);
socket.on("RENDER_ROOM", renderRoom);
socket.on("RELOAD_STATUS", updateStatus);
socket.on("RELOAD_ONLINE", reloadOnline);
socket.on("GAME_STARTING", startingTheGame);
socket.on("UPDATE_STATUS_GREEN", updateStatusIndGreen);
socket.on("UPDATE_STATUS_RED", updateStatusIndRed);
socket.on("UPDATE_STATUS_GREEN_ALL", updateStatusIndGreenAll);
socket.on("UPDATE_STATUS_RED_ALL", updateStatusIndRedAll);

//commentator sockets
socket.on("COMMENTATOR_SAYS_POSITION", commentatorPosition);
socket.on("TELL_A_JOKE", jokeTime);
socket.on("USER_30_LETTERS_TO_WIN", notifyAboutPlayerToWin);
socket.on("USER_FINISHED", notifyAboutPlayerFinished);

socket.on("SHOW_WINNERS", showWinners);
