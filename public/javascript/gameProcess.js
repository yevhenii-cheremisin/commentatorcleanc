let displayElement = document.createElement("div");
let inputElement = document.createElement("textarea");
let timerElement = document.createElement("div");
let timeTimer;
let gameTimer;
let socket;
let roomname;
let username;
let letterInText = 0;
let currentTyped = 0;
const timeToWaitBeforeAction = 10;
let percent = 0;

export function gameProcess(
  dataBlocks,
  dataConst,
  text,
  Usersocket,
  Userusername,
  Userroomname
) {
  timeCommentator();
  const { display, textArea, timer } = dataBlocks;
  const { timerTime, gameTime } = dataConst;
  gameTimer = timerTime;
  timeTimer = gameTime;

  socket = Usersocket;
  username = Userusername;
  roomname = Userroomname;
  displayElement = display;
  inputElement = textArea;
  timerElement = timer;
  getNextQuote(text);
  let cansent = true;
  let isEnd;
  inputElement.addEventListener("input", () => {
    console.log(letterInText);
    let isEnd = true;
    const arrayQuote = displayElement.querySelectorAll("span");
    const arrayValue = inputElement.value.split("");
    arrayQuote.forEach((characterSpan, index) => {
      const character = arrayValue[index];
      if (character == null) {
        characterSpan.classList.remove("correct");
        characterSpan.classList.remove("incorrect");
        isEnd = false;
      } else if (character === characterSpan.innerText) {
        characterSpan.classList.add("correct");
        characterSpan.classList.remove("incorrect");
        if (arrayQuote[index + 1]) {
          arrayQuote[index + 1].classList.add("next-letter");
        }
        arrayQuote[index].classList.remove("next-letter");
      } else {
        characterSpan.classList.remove("correct");
        characterSpan.classList.add("incorrect");
        isEnd = false;
      }
    });
    currentTyped = document.getElementsByClassName("correct").length;
    console.log(letterInText - currentTyped + " left");
    percent = currentTyped / letterInText;
    console.log(currentTyped);

    if (letterInText - currentTyped == 30) {
      socket.emit("30_LETTERS_TO_WIN", { username, roomname });
    }

    if (isEnd && cansent) {
      cansent = false;
      checkFor();
      currentTyped = 0;
    }

    socket.emit("UPDATE_BAR", { username, percent, roomname });
  });
}

function timeCommentator() {
  let timeCurrent = 0;
  const interval = setInterval(() => {
    if (timeCurrent % 30 == 0 && timeCurrent > timeToWaitBeforeAction) {
      socket.emit("COMMENTATOR_LAUNCH", { roomname, username, percent });
    }
    if (timeCurrent > timeTimer) {
      clearInterval(interval);
    }
    if (timeCurrent % 20 == 0 && timeCurrent > timeToWaitBeforeAction) {
      socket.emit("JOKE_TIME", roomname);
    }
    timeCurrent++;
  }, 10000);
}

function checkFor() {
  console.log("checking");
  socket.emit("FINISHED", roomname);
}

let startTime;
function startTimer() {
  timerElement.innerText = timeTimer + " seconds left";
  startTime = new Date();
  const interval = setInterval(() => {
    let time = getTimerTime();
    timerElement.innerText = time + " seconds left";

    if (time == 0) {
      checkFor();
      clearInterval(interval);
    }
  }, 200);
}

function getTimerTime() {
  return Math.floor(timeTimer - (new Date() - startTime) / 1000);
}

function getNextQuote(text) {
  //console.log(text);
  const quote = text;
  displayElement.innerHTML = "";
  quote.split("").forEach((character) => {
    letterInText++;
    const characterSpan = document.createElement("span");
    characterSpan.innerText = character;
    displayElement.appendChild(characterSpan);
  });
  inputElement.innerText = null;
  startTimer();
}
