const username = sessionStorage.getItem("username");
const socket = io("", { query: { username } });
const room_page = document.getElementById("rooms-page");
const game_page = document.getElementById("game-page");
const roomBlocks = document.createElement("div");
const nameroom = document.createElement("div");
nameroom.classList.add("looby_name");
const statuses = document.createElement("div");

const container = document.createElement("div");
container.classList.add("container");

const divInfoBlock = document.createElement("div");
const divGameBlock = document.createElement("div");

let user;

roomBlocks.classList.add("roomblocks");
const rooms = ["default #1"];
let roomID = 1;

if (!username) {
  window.location.replace("/login");
}

const render = () => {
  createButtonCreateRoom();
  room_page.appendChild(roomBlocks);
  createRoomBlock("default #1");
};

const createButtonCreateRoom = () => {
  const buttonCont = document.createElement("button");
  buttonCont.addEventListener("click", createCustomRoomBlock);
  buttonCont.id = "add-room-btn";
  buttonCont.classList.add("btn");
  buttonCont.textContent = "Create Room";
  room_page.appendChild(buttonCont);
};

const createCustomRoomBlock = () => {
  const name = prompt("Enter game name: ");
  if (rooms.includes(name)) {
    alert("Wrong name");
  } else {
    rooms.push(name);
    socket.emit("CREATE_BLOCK", name);
  }
};

const createRoomBlock = (name) => {
  const divBlock = document.createElement("div");
  divBlock.classList.add("room");
  let classBe = "room" + roomID;
  roomID++;
  divBlock.classList.add("blockGame", classBe);
  const header = document.createElement("div");
  header.classList.add("headerGame");
  header.textContent = "1 user connected";
  const nameRoom = document.createElement("div");
  nameRoom.classList.add("nameRoom");
  nameRoom.innerHTML = name;
  const buttonCont = document.createElement("button");
  buttonCont.classList.add("btn", "btn-join");
  buttonCont.textContent = "Join Room";
  buttonCont.addEventListener("click", function () {
    joinRoom(classBe);
  });
  divBlock.appendChild(header);
  divBlock.appendChild(nameRoom);
  divBlock.appendChild(buttonCont);
  roomBlocks.appendChild(divBlock);
  createGameBlock(classBe);
  if (name != "default #1") {
    joinRoom(classBe);
  }
};

const createGameBlock = (roomID) => {
  nameroom.textContent = roomID;
  const buttonBack = document.createElement("button");
  buttonBack.classList.add("btn", "btn-back");
  buttonBack.textContent = "Back";
  buttonBack.addEventListener("click", function () {
    exitRoom(roomID);
  });
  divInfoBlock.appendChild(nameroom);
  divInfoBlock.appendChild(buttonBack);
  const gameSpace = document.createElement("div");
  gameSpace.classList.add("gamespace");

  const readyBut = document.createElement("button");
  readyBut.classList.add("btn");
  readyBut.id = roomID + "but";
  readyBut.textContent = "Ready";
  readyBut.addEventListener("click", function () {
    ready(roomID);
  });
  gameSpace.appendChild(readyBut);

  divGameBlock.appendChild(gameSpace);
  container.appendChild(divInfoBlock);
  container.appendChild(divGameBlock);
  game_page.appendChild(container);
};

const ready = (roomID) => {
  let idBut = roomID + "but";
  const but = document.getElementById(idBut);
  let idInd = user + "ind";
  const indicator = document.getElementById(idInd);
  if (but.textContent == "Ready") {
    but.textContent = "Not Ready";
    indicator.classList.remove("indicator-red");
    indicator.classList.add("indicator-green");
  } else {
    but.textContent = "Ready";
    indicator.classList.add("indicator-red");
    indicator.classList.remove("indicator-green");
  }
};

const joinRoom = (id) => {
  socket.emit("JOIN_ROOM", { roomID: id, nameuser: username });
  console.log("successfully joined to the room");
  room_page.classList.add("display-none");
  game_page.classList.remove("display-none");
};

const exitRoom = (room) => {
  socket.emit("EXIT_ROOM", { sid: socket.id, user: username, roomID: room });
  console.log("successfully exited from the room");
  room_page.classList.remove("display-none");
  game_page.classList.add("display-none");
};

const createStatus = (name) => {
  user = name;
  const statusBlock = document.createElement("div");
  const upStatus = document.createElement("div");
  upStatus.classList.add("upstatus");

  const indicator = document.createElement("div");
  indicator.classList.add("indicator-red");
  indicator.id = username + "ind";

  const nameStatus = document.createElement("div");
  nameStatus.textContent = name;

  upStatus.appendChild(indicator);
  upStatus.appendChild(nameStatus);

  const bar = document.createElement("div");
  bar.classList.add("bar");

  statusBlock.append(upStatus);
  statusBlock.append(bar);
  statusBlock.classList.add(name);
  statuses.appendChild(statusBlock);
  divInfoBlock.appendChild(statuses);
};

function removeAllChildNodes(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}

const updateUsers = (obj) => {
  const { arr, room } = obj;
  let connectedUsers = new Map(JSON.parse(arr));
  console.log(connectedUsers);
  removeAllChildNodes(statuses);
  connectedUsers.forEach((value, key) => {
    console.log(value + " == " + room);
    if (value == room) {
      createStatus(key);
    }
  });
};

const placeLocked = (name) => {
  window.location.replace("/login"); //NOT WORKING
  alert("User " + name + " exists!");
};

const cleanStatus = (ids) => {
  const usersBlock = document.getElementsByClassName(ids);
  usersBlock[0].classList.add("display-none");
};

socket.on("USER_EXISTS", placeLocked);
socket.on("RENDER", render);
socket.on("CREATE_NEW_ROOM", createRoomBlock);

socket.on("UPDATE_ROOM", updateUsers);
socket.on("USER_LEFT", cleanStatus);
//const { roomname, timerSec, gameSec } = data;
//let dataBlocks = { display: display, textArea: textArea, timer };
//let dataConst = { timerTime: timerSec, gameTime: gameSec };
