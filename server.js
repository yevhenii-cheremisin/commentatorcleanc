import express from "express";
import http from "http";
import socketIO from "socket.io";
import socketHandler from "./socket";
import routes from "./routes";
import { STATIC_PATH, PORT } from "./config";
import texts from "./data";

const app = express();
const httpServer = http.Server(app);
const io = socketIO(httpServer);

app.use(express.static(STATIC_PATH));

// app.use("/game/texts/:id", (req, res) => {
//   console.log(texts.texts[req.params.id]);
//   let data = texts.texts[req.params.id];
//   res.status(200).send({ data });
// });

routes(app);

app.get("*", (req, res) => {
  res.redirect("/login");
});

socketHandler(io);

httpServer.listen(process.env.PORT || PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});
